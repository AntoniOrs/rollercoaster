```
  ____   ___  _     _     _____ ____
 |  _ \ / _ \| |   | |   | ____|  _ \
 | |_) | | | | |   | |   |  _| | |_) |
 |  _ <| |_| | |___| |___| |___|  _ <
 |_|_\_\\___/|_____|_____|_____|_|_\_\___
  / ___/ _ \  / \  / ___|_   _| ____|  _ \
 | |  | | | |/ _ \ \___ \ | | |  _| | |_) |
 | |__| |_| / ___ \ ___) || | | |___|  _ <
  \____\___/_/   \_\____/ |_| |_____|_| \_\
```

Usage: rollercoaster [options]

Simply update android track rollout

## Options:

-   `-p, --packageName <packageName>` the package name, example: com.android.sample (env: `PACKAGE_NAME`)
-   `-c, --credentials <credentials>` credentials (env: `GOOGLE_APPLICATION_CREDENTIALS`)
-   `-k, --keyFile <keyFile>` keyFile (env: `GOOGLE_APPLICATION_CREDENTIALS_PATH`)
-   `-t, --track <track>` track on which make changes (default: `"production"`, env: `TRACK`)
-   `-r, --rolloutType <rolloutType>` type of the rollout (choices: `"even"`, `"ios"`, `"exponential"`, `"tens"`, default: `"even"`, env: `ROLLOUT_TYPE`)
-   `-n, --no-logo` hide logo
-   `-s, --silent` hide output (default: `false`, env: `SILENT`)
-   `-d, --dry` don't push changes (default: `false`)
-   `-v, --verbose` verbosity that can be increased (levels: `[ 0, 1, 2, 3 ]`)
-   `-V, --version` output the version number
-   `-h, --help` display help for command

### Possible rollout types procentages:

```
even:         [   14,  29,  43,  57,  71,  86, 100 ]
exponential:  [    2,   3,   6,  13,  25,  50, 100 ]
ios:          [    1,   2,   5,  10,  20,  50, 100 ]
tens:         [   10,  20,  30,  40,  50,  60,  70,  80,  90, 100 ]
```
