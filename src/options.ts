import { Logger, VERBOSITY_LEVELS } from "./util/logger";
import { RolloutType } from "./changeRollout";

export const CONFIDENTIAL = "*****CONFIDENTIAL*****";

export type IOptions = {
	packageName: string;
	credentials?: string;
	keyFile?: string;
	track: string;
	dry: boolean;
	verbose: number;
	rolloutType: keyof typeof RolloutType;
	silent: boolean;
	logo: boolean;
};

export class Options {
	public packageName: string;
	public keyFile?: string;
	#credentials?: string;
	private credentials?: string;
	public track: string;
	public dry: boolean;
	public verbose: number;
	public rolloutType: keyof typeof RolloutType;
	public silent: boolean;
	public logo: boolean;

	getCredentials() {
		return this.#credentials;
	}

	setCredentials(value: string | undefined) {
		this.#credentials = value;
		this.credentials = value ? CONFIDENTIAL : undefined;
	}

	constructor({
		packageName,
		credentials,
		hiddenCredentials,
		keyFile,
		track,
		dry,
		verbose,
		rolloutType,
		logo,
		silent,
	}: IOptions & { hiddenCredentials?: string }) {
		const _credentials = credentials ?? hiddenCredentials;
		if (!keyFile && !_credentials) {
			throw new Error(
				"Either option '-c, --credentials <credentials>' or '-k, --keyFile <keyFile>' must be set."
			);
		}

		this.packageName = packageName;
		this.#credentials = _credentials;
		this.credentials = _credentials ? CONFIDENTIAL : undefined;
		this.keyFile = keyFile;
		this.track = track;
		this.dry = dry;
		this.verbose = Math.min(verbose, Math.max(...VERBOSITY_LEVELS));
		this.rolloutType = rolloutType;
		this.logo = logo;
		this.silent = silent;
		Logger.level = verbose;
		Logger.silent = silent;
	}
}
