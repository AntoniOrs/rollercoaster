import { Logger, Verbosity } from "./logger";
import { CONFIDENTIAL } from "../options";
import util from "util";

type InspectOptions = {
	replacer?: (this: unknown, key: string, value: unknown) => unknown;
} & util.InspectOptions;

export function inspect(object: unknown, options?: InspectOptions): string {
	const inner = JSON.parse(
		JSON.stringify(
			object,
			options?.replacer
				? options?.replacer
				: (key, value) =>
						key === "Authorization" ? CONFIDENTIAL : value
		)
	);

	return util.inspect(inner, {
		colors: true,
		compact: false,
		depth: null,
		...options,
	});
}

export function inspectHideReleaseNotes(
	object: unknown,
	options?: InspectOptions
): string {
	return inspect(object, {
		...options,
		replacer:
			Logger.level >= Verbosity.moreInfo
				? (key, value) =>
						key === "Authorization" ? CONFIDENTIAL : value
				: (key, value) => {
						if (key === "releaseNotes") {
							return undefined;
						} else if (key === "Authorization") {
							return CONFIDENTIAL;
						} else {
							return value;
						}
				  },
	});
}
