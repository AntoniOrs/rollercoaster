import { Options } from "../options";
import chalk from "chalk";
import figlet from "figlet";

export const LOGO = chalk.blue(
	figlet.textSync("ROLLERCOASTER", { horizontalLayout: "full" })
);

export function showLogo(options: Options) {
	if (options.logo && !options.silent) {
		console.log(LOGO);
	}
}
