import rootConsola, { Consola as ConsolaType } from "consola";

type LoggerMethod = (
	level: number | unknown,
	message?: unknown,
	...optionalParams: unknown[]
) => void;

export enum Verbosity {
	default = 0,
	info = 1,
	moreInfo = 2,
	full = 3,
}

export const VERBOSITY_LEVELS: number[] = Object.values(Verbosity).filter((v) =>
	Number.isInteger(v)
) as number[];

export class Logger {
	private consola: ConsolaType;
	public static level = 0;
	public static silent = false;

	constructor(public tag: string, public level?: number) {
		this.consola = rootConsola.withTag(tag);
	}

	static checkLevel(input: unknown): input is number {
		if (input !== null && input !== undefined) {
			if (typeof input === "number") {
				return !Number.isNaN(input);
			} else if (typeof input === "string") {
				return !Number.isNaN(parseInt(input, 10));
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	runConsolaFunction(
		method: "info" | "success" | "warn" | "error",
		level: number | unknown,
		...optionalParams: unknown[]
	) {
		if (Logger.silent) {
			return;
		}
		if (Logger.checkLevel(level)) {
			if (Logger.level >= level) {
				const firstElement = optionalParams?.shift();
				this.consola[method]?.(firstElement, ...optionalParams);
			}
		} else {
			if (Logger.checkLevel(this.level)) {
				if (Logger.level >= this.level) {
					this.consola[method]?.(level, ...optionalParams);
				}
			} else {
				this.consola[method]?.(level, ...optionalParams);
			}
		}
	}

	info: LoggerMethod = (...params) =>
		this.runConsolaFunction("info", ...params);
	warn: LoggerMethod = (...params) =>
		this.runConsolaFunction("warn", ...params);
	success: LoggerMethod = (...params) =>
		this.runConsolaFunction("success", ...params);
	error: LoggerMethod = (...params) =>
		this.runConsolaFunction("error", ...params);
}
