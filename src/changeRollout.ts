import { Logger } from "./util/logger";
import { androidpublisher_v3 } from "googleapis/build/src/apis/androidpublisher/v3";
import chalk from "chalk";

export enum RolloutType {
	even = "even",
	ios = "ios",
	exponential = "exponential",
	tens = "tens",
}

export const THRESHOLDS: Record<RolloutType, Array<number>> = {
	even: [0.142857, 0.285714, 0.428571, 0.571429, 0.714286, 0.857143, 1],
	exponential: [0.015625, 0.03125, 0.0625, 0.125, 0.25, 0.5, 1],
	ios: [0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1],
	tens: [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
};

let thresholdsString = "";
const thresholdsKeysMaxLenght =
	Math.max(...Object.keys(THRESHOLDS).map((key) => key.length)) + 3;

for (const key in THRESHOLDS) {
	if (Object.prototype.hasOwnProperty.call(THRESHOLDS, key)) {
		const element = THRESHOLDS[key as RolloutType].map((e) =>
			Math.round(e * 100)
		);
		thresholdsString +=
			(key + ":").padEnd(thresholdsKeysMaxLenght) +
			"[ " +
			element.map((e) => chalk.yellow(String(e).padStart(4))) +
			" ]" +
			"\n";
	}
}

export const THRESHOLDS_STRING = thresholdsString;

const logger = new Logger("changeRollout");

function round(input: number) {
	return Math.round((input + Number.EPSILON) * 100) / 100;
}

export function changeRollout(
	release: androidpublisher_v3.Schema$TrackRelease,
	type: RolloutType | keyof typeof RolloutType
) {
	if ("userFraction" in release) {
		const rolloutPercentage = release["userFraction"];
		if (
			rolloutPercentage === null ||
			rolloutPercentage === undefined ||
			Number.isNaN(rolloutPercentage)
		) {
			throw new Error("Roll out procentage must be a number.");
		}
		if (rolloutPercentage === 0) {
			logger.info("Release not rolled out yet");
		} else if (rolloutPercentage < 1) {
			const next = THRESHOLDS[type].find(
				(element) => round(element) > rolloutPercentage
			);
			if (next !== undefined) {
				if (next === 1) {
					delete release["userFraction"];
					release["status"] = "completed";
				} else {
					release["userFraction"] = round(next);
				}
			}
		} else {
			logger.info("Release already fully rolled out");
		}
	}
}
