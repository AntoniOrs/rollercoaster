import { Logger, Verbosity } from "./util/logger";
import { inspect, inspectHideReleaseNotes } from "./util/inspect";
import { Options } from "./options";
import chalk from "chalk";
import { changeRollout } from "./changeRollout";
import { google } from "googleapis";
import { isEqual } from "lodash";

export async function main(options: Options) {
	const { track, packageName, dry, rolloutType, keyFile } = options;
	const credentials = options.getCredentials();
	const logger = new Logger("main");
	const googleLogger = new Logger("google", Verbosity.full);

	const auth = new google.auth.GoogleAuth({
		credentials: credentials ? JSON.parse(credentials) : undefined,
		keyFile,
		scopes: ["https://www.googleapis.com/auth/androidpublisher"],
	});

	google.options({ auth });

	const service = google.androidpublisher("v3");

	const editsInsertResponse = await service.edits.insert({
		packageName,
	});
	const {
		data: { id: editId },
	} = editsInsertResponse;
	googleLogger.info(inspect(editsInsertResponse, { compact: true }));

	if (editId) {
		const tracksGetResponse = await service.edits.tracks.get({
			editId,
			packageName,
			track,
		});
		googleLogger.info(inspect(tracksGetResponse, { compact: true }));
		const { data: trackResult } = tracksGetResponse;
		const oldResult = JSON.parse(JSON.stringify(trackResult));
		logger.info("Current state:", inspectHideReleaseNotes(oldResult));
		if (trackResult.releases) {
			for (const release of trackResult.releases) {
				changeRollout(release, rolloutType);
			}
			const completed_releases = trackResult.releases.filter(
				({ status }) => status === "completed"
			);
			if (completed_releases.length === 2) {
				trackResult.releases.pop();
			}

			logger.info("Updated state:", inspectHideReleaseNotes(trackResult));
			if (isEqual(trackResult, oldResult)) {
				const editsDeleteResonse = await service.edits.delete({
					editId,
					packageName,
				});
				googleLogger.info(
					inspect(editsDeleteResonse, { compact: true })
				);
				logger.success(
					"States are equal, there's no need for commiting"
				);
			} else {
				const tracksUpdateResponse = await service.edits.tracks.update({
					editId,
					packageName,
					requestBody: trackResult,
					track,
				});
				googleLogger.info(
					inspect(tracksUpdateResponse, { compact: true })
				);
				if (dry) {
					const editsDeleteResonse = await service.edits.delete({
						editId,
						packageName,
					});
					googleLogger.info(inspect(editsDeleteResonse));
					logger.warn(
						"Edit",
						chalk.green(editId),
						"was",
						chalk.red("not"),
						"committed, run without",
						chalk.yellow("--dry"),
						"flag to commit your changes."
					);
				} else {
					const editsCommitResponse = await service.edits.commit({
						editId,
						packageName,
					});
					googleLogger.info(
						inspect(editsCommitResponse, { compact: true })
					);
					logger.success(
						"Edit",
						chalk.green(editsCommitResponse.data.id),
						"has been committed"
					);
				}
			}
		}
	}
}
