#!/usr/bin/env node
import { LOGO, showLogo } from "./util/logo";
import { Logger, VERBOSITY_LEVELS, Verbosity } from "./util/logger";
import { Option, program } from "commander";
import { RolloutType, THRESHOLDS_STRING } from "./changeRollout";
import { description, name, version } from "../package.json";
import { Options } from "./options";
import { inspect } from "./util/inspect";
import { main } from "./main";

const logger = new Logger("options", Verbosity.info);

const programObject = program
	.name(name)
	.description(description)
	.addHelpText("beforeAll", ({ error }) => (error ? "" : LOGO))
	.addOption(
		new Option(
			"-p, --packageName <packageName>",
			"the package name, example: com.android.sample"
		)
			.default(process.env.PACKAGE_NAME)
			.env("PACKAGE_NAME")
			.makeOptionMandatory()
	)
	.addOption(
		new Option(
			"--hidden-credentials <hiddenCredentials",
			"hiddenCredentials"
		)
			.default(process.env.GOOGLE_APPLICATION_CREDENTIALS)
			.env("GOOGLE_APPLICATION_CREDENTIALS")
			.hideHelp()
	)
	.addOption(
		new Option("-c, --credentials <credentials>", "credentials")
			.env("GOOGLE_APPLICATION_CREDENTIALS")
			.conflicts("keyFile")
	)
	.addOption(
		new Option("-k, --keyFile <keyFile>", "keyFile")
			.default(process.env.GOOGLE_APPLICATION_CREDENTIALS_PATH)
			.env("GOOGLE_APPLICATION_CREDENTIALS_PATH")
			.conflicts("credentials")
	)
	.addOption(
		new Option("-t, --track <track>", "track on which make changes")
			.default(process.env.TRACK ?? "production")
			.env("TRACK")
			.makeOptionMandatory()
	)
	.addOption(
		new Option("-r, --rolloutType <rolloutType>", "type of the rollout")
			.choices(Object.values(RolloutType))
			.default(process.env.ROLLOUT_TYPE ?? RolloutType.even)
			.env("ROLLOUT_TYPE")
	)
	.addOption(new Option("-n, --no-logo", "hide logo").default(true))
	.addOption(
		new Option("-s, --silent", "hide output").default(false).env("SILENT")
	)
	.option("-d, --dry", "don't push changes", false)
	.addOption(
		new Option(
			"-v, --verbose",
			"verbosity that can be increased (levels: " +
				inspect(VERBOSITY_LEVELS, { compact: true }) +
				")"
		)
			.default(0)
			.argParser((_, previous: number) => previous + 1)
	)
	.addHelpText(
		"afterAll",
		"\nPossible rollout types procentages:\n" + THRESHOLDS_STRING
	)
	.version(version)
	.showHelpAfterError()
	.parse(process.argv);

try {
	if (process.argv.join("").includes("--hidden-credentials")) {
		logger.error(Verbosity.default, "You can not pass this option.");
		programObject.help({ error: true });
		process.exit(1);
	}
	const options = new Options(program.opts());
	showLogo(options);
	logger.info(`Version: ${version}`);
	const max = Math.max(...VERBOSITY_LEVELS);
	if (program.opts().verbose > options.verbose) {
		logger.warn(
			0,
			`Max verbosity is ${max}, there is no need for setting it higher than that.`
		);
	}
	logger.info(options);

	main(options).catch((error) => {
		new Logger("main").error(error);
		process.exit(1);
	});
} catch (error) {
	if (error instanceof Error) {
		logger.error(Verbosity.default, error.message);
		programObject.help({ error: true });
	} else {
		logger.error(Verbosity.default, error);
	}
}
